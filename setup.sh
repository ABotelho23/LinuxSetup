#!/bin/bash
echo "This script will update your system."
echo "This script will install a large amount of software and configure many parameters."
echo "This script will only remove a handful of applications."
echo "Please remove linuxsetup folder once setup.sh has finished running."

#prompt
read -r -p "Are you sure you want to run this script? [y/N] " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]
then

sudo --preserve-env=HOME bash setupsystem.sh

bash setupuser.sh

read -r -p "COMPLETE! REBOOT NOW? [y/N] " rebootnow
if [[ "$rebootnow" =~ ^([yY][eE][sS]|[yY])+$ ]]
then
echo "Rebooting!"
sudo reboot
else
echo "Reboot not selected. Please ensure you reboot at a later time."
fi

else
  echo "Ok, exiting..."
fi
