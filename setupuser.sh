#!/bin/bash

#create autostart user directory
mkdir $HOME/.config/autostart

#Android Messages setup
mkdir -p $HOME/.config/android-messages-desktop/
touch $HOME/.config/android-messages-desktop/Settings
echo '{"trayEnabledPref":true,"startInTrayPref":true,"autoHideMenuPref":false,"pressEnterToSendPref":true,"notificationSoundEnabledPref":true,"hideNotificationContentPref":false,"useSystemDarkModePref":true}' > $HOME/.config/android-messages-desktop/Settings
AndroidMessages &
killall AndroidMessages
cp settings/settings.json $HOME/.config/android-messages-desktop/settings.json
cp /usr/share/applications/AndroidMessages.desktop $HOME/.config/autostart/AndroidMessages.desktop

#Signal setup
#signal messenger setup
mkdir -p $HOME/.local/share/applications
cat desktopfiles/signaltrayicon.txt > $HOME/.local/share/applications/org.signal.Signal.desktop
mkdir -p $HOME/.config/autostart
cat desktopfiles/signalautostart.txt > $HOME/.config/autostart/org.signal.Signal.desktop

bash gnome-shell-extension-installer 307 3.36 #Dash to Dock
bash gnome-shell-extension-installer 1007 3.36 #Remove Window is Ready
bash gnome-shell-extension-installer 750 3.36 #OpenWeather
bash gnome-shell-extension-installer 1036 3.36 #Extensions
bash gnome-shell-extension-installer 19 3.36 #User Themes

#Enable other Gnome extensions
gsettings set org.gnome.shell enabled-extensions "['dash-to-dock@micxgx.gmail.com','windowIsReady_Remover@nunofarruca@gmail.com','openweather-extension@jenslody.de','extensions@abteil.org','user-theme@gnome-shell-extensions.gcampax.github.com','noannoyance@sindex.com','ubuntu-appindicators@ubuntu.com']"

#Set theme, icons, fonts, settings
gsettings set org.gnome.desktop.interface gtk-theme Plata-Compact
gsettings set org.gnome.desktop.wm.preferences theme Plata-Compact
gsettings set org.gnome.shell.extensions.user-theme name Plata-Compact
gsettings set org.gnome.desktop.interface font-name "Noto Sans Regular 11"
gsettings set org.gnome.desktop.interface monospace-font-name "Noto Sans Mono Regular 11"
gsettings set org.gnome.desktop.interface document-font-name "Arial Regular 11"
gsettings set org.gnome.desktop.wm.preferences titlebar-font "Noto Sans Bold 11"
gsettings set org.gnome.desktop.interface icon-theme Papirus
gsettings set org.gnome.desktop.interface enable-hot-corners false
gsettings set org.gnome.desktop.interface clock-format 24h
gsettings set org.gnome.desktop.interface clock-show-date true
gsettings set org.gnome.desktop.interface show-battery-percentage true
gsettings set org.gnome.shell always-show-log-out true

#Dock config
gsettings set org.gnome.shell.extensions.dash-to-dock preferred-monitor 0
gsettings set org.gnome.shell.extensions.dash-to-dock dock-position 'BOTTOM'
gsettings set org.gnome.shell.extensions.dash-to-dock transparency-mode 'DYNAMIC'
gsettings set org.gnome.shell.extensions.dash-to-dock icon-size-fixed true
gsettings set org.gnome.shell.extensions.dash-to-dock dash-max-icon-size 48
gsettings set org.gnome.shell.extensions.dash-to-dock multi-monitor true
gsettings set org.gnome.shell.extensions.dash-to-dock running-indicator-style 'SEGMENTED'
gsettings set org.gnome.shell.extensions.dash-to-dock running-indicator-dominant-color true
gsettings set org.gnome.shell.extensions.dash-to-dock show-trash true
gsettings set org.gnome.shell.extensions.dash-to-dock show-show-apps-button true
gsettings set org.gnome.shell.extensions.dash-to-dock force-straight-corner true
gsettings set org.gnome.shell.extensions.dash-to-dock background-color '#ffffff'
gsettings set org.gnome.shell.extensions.dash-to-dock pressure-threshold 100.0
gsettings set org.gnome.shell.extensions.dash-to-dock click-action 'cycle-windows'
gsettings set org.gnome.shell.extensions.dash-to-dock middle-click-action 'launch'
gsettings set org.gnome.shell.extensions.dash-to-dock scroll-action 'cycle-windows'
gsettings set org.gnome.shell.extensions.dash-to-dock scroll-switch-workspace false
gsettings set org.gnome.shell.extensions.dash-to-dock intellihide true
gsettings set org.gnome.shell.extensions.dash-to-dock custom-theme-shrink true
gsettings set org.gnome.shell.extensions.dash-to-dock height-fraction 0.90
gsettings set org.gnome.shell.extensions.dash-to-dock show-mounts true
