# LinuxSetup
### My personal script for setting up new Ubuntu installs.
### Designed and Tested to work on Ubuntu 20.04.

Clone the repository:
`sudo apt install git -y && git clone https://gitlab.com/ABotelho23/LinuxSetup.git`

Navigate to the correct directory:
`cd LinuxSetup`

Launch the setup:

`./setup.sh`

Do NOT run the script with sudo. You will be prompted for a sudo password to run the component that requires admin permissions.

The script will have a few prompts at the beginning, and then at the end. It will finally prompt to reboot, which I recommend.

Please see files in the aptlist directory for which packages will be installed by each prompt option in the script.
