[Desktop Entry]
Name=Signal
Exec=flatpak run org.signal.Signal --start-in-tray %U
Terminal=false
Type=Application
Icon=org.signal.Signal
StartupWMClass=Signal
Comment=Private messaging from your desktop
MimeType=x-scheme-handler/sgnl;
Categories=Network;InstantMessaging;Chat;
X-Flatpak-RenamedFrom=signal-desktop.desktop;
