#!/bin/bash

#prompt
read -r -p "Perform power-user software installs? [y/N] " poweruser
read -r -p "Perform creation software installs? [y/N] " creation

echo "Starting script! Please do not stop this script once it has started."

#disable auto updates/upgrades check
systemctl disable --now apt-daily{,-upgrade}.{timer,service}
sudo killall dpkg

#enable 32-bit
sudo dpkg --add-architecture i386

#do updates and software upgrades
sudo apt update
sudo apt upgrade -y

#terminator bleeding edge ppa
sudo add-apt-repository ppa:mattrose/terminator -y
sudo apt update

#removals
sudo apt install tilix terminator -y
sudo apt remove byobu gnome-terminal evince eog geary -y
sudo apt autoremove -y #clean up after removals

#prepare environment
mkdir deb

#essential installs
xargs -a aptlist/essential sudo apt install -y
sudo usermod -a -G kvm $SUDO_USER

if [[ "$poweruser" =~ ^([yY][eE][sS]|[yY])+$ ]]
then
#power-user installs

#vscodium
wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg | gpg --dearmor | sudo dd of=/etc/apt/trusted.gpg.d/vscodium.gpg
echo 'deb https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/debs/ vscodium main' | sudo tee --append /etc/apt/sources.list.d/vscodium.list

sudo apt-add-repository ppa:maarten-fonville/android-studio -y #android studio

sudo add-apt-repository ppa:cappelikan/ppa -y # mainline kernels tool

sudo apt update
xargs -a aptlist/power-user sudo apt install -y
sudo pip3 install WoeUSB-ng

#Flutter SDK
wget --show-progress "https://storage.googleapis.com/flutter_infra/releases/stable/linux/flutter_linux_2.0.5-stable.tar.xz" -O ./deb/fluttersdk.tar.xz
tar xf ./deb/fluttersdk.tar.xz
sudo cp -R ./flutter /opt
echo 'export PATH="$PATH":/opt/flutter/bin' >> $HOME/.bashrc
sudo chown -R $SUDO_USER:root /opt/flutter
sudo chmod -R 774 /opt/flutter
sudo -u $SUDO_USER /opt/flutter/bin/flutter precache

fi

if [[ "$creation" =~ ^([yY][eE][sS]|[yY])+$ ]]
then
#creation installs
xargs -a aptlist/creation sudo apt install -y
fi

#new ppa/repo adds
sudo add-apt-repository ppa:unit193/encryption -y #veracrypt
sudo add-apt-repository ppa:papirus/papirus -y #papirus icons
sudo add-apt-repository ppa:tista/plata-theme -y #plata theme
sudo add-apt-repository ppa:andreasbutti/xournalpp-master -y #Xournal++

#OnlyOffice
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys CB2DE8E5
echo "deb https://download.onlyoffice.com/repo/debian squeeze main" | sudo tee -a /etc/apt/sources.list.d/onlyoffice.list

#update after repo adds
sudo apt update

#new ppa/repo installs
sudo apt install onlyoffice-desktopeditors -y
sudo apt install papirus-icon-theme -y
sudo apt install veracrypt -y
sudo apt install plata-theme -y
sudo apt install xournalpp -y

#Flatpak installs
sudo apt install flatpak gnome-software-plugin-flatpak -y
sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
sudo flatpak install flathub com.bitwarden.desktop --system -y -v --noninteractive
sudo flatpak install flathub org.signal.Signal --system -y -v --noninteractive

#android messages setup
wget --show-progress "https://github.com/OrangeDrangon/android-messages-desktop/releases/download/v5.1.1/AndroidMessages-v5.1.1-linux-amd64.deb" -O ./deb/androidmessages.deb
sudo gdebi -n ./deb/androidmessages.deb

sudo cp -R ./desktopfiles/* /usr/share/applications
sudo cp -R ./icons/* /usr/share/icons
sudo update-desktop-database /usr/share/applications

#fix time
timedatectl set-local-rtc 1

#fix open in terminal for tilix
terminator &
killall terminator
gsettings set org.gnome.desktop.default-applications.terminal exec terminator

#MS fonts
sudo apt install ttf-mscorefonts-installer -y
sudo fc-cache -f -v

#tilix fix
#echo 'if [[ $TILIX_ID ]]; then' >> /home/$SUDO_USER/.bashrc
#echo 'source /etc/profile.d/vte.sh' >> /home/$SUDO_USER/.bashrc
#echo 'fi' >> /home/$SUDO_USER/.bashrc
#ln -s /etc/profile.d/vte-2.91.sh /etc/profile.d/vte.sh

#neofetch at terminal start
echo 'neofetch' >> /etc/bash.bashrc

#purple background fix
sudo update-alternatives --install /usr/share/gnome-shell/gdm3-theme.gresource gdm3-theme.gresource /usr/share/gnome-shell/gnome-shell-theme.gresource 100

#Disable/Remove Ubuntu Dock and Desktop Icons
gsettings set org.gnome.shell disabled-extensions "['ubuntu-dock@ubuntu.com','desktop-icons@csoriano']"
sudo rm -R /usr/share/gnome-shell/extensions/desktop-icons@csoriano
sudo rm -R /usr/share/gnome-shell/extensions/ubuntu-dock@ubuntu.com

#Install other Gnome extensions
wget -O gnome-shell-extension-installer "https://github.com/brunelli/gnome-shell-extension-installer/raw/master/gnome-shell-extension-installer"
chmod +x gnome-shell-extension-installer
sudo bash gnome-shell-extension-installer 307 3.36 #Dash to Dock
sudo bash gnome-shell-extension-installer 1007 3.36 #Remove Window is Ready
sudo bash gnome-shell-extension-installer 750 3.36 #OpenWeather
sudo bash gnome-shell-extension-installer 1036 3.36 #Extensions
sudo bash gnome-shell-extension-installer 19 3.36 #User Themes
sudo cp /usr/share/gnome-shell/extensions/user-theme@gnome-shell-extensions.gcampax.github.com/schemas/org.gnome.shell.extensions.user-theme.gschema.xml /usr/share/glib-2.0/schemas
sudo glib-compile-schemas /usr/share/glib-2.0/schemas

#clean up
#Remove SNAPS FOREVER!
sudo apt remove --purge snapd -y

#Disable pppd-dns service
sudo systemctl disable pppd-dns.service

# Re-add Gnome store, remove snap plugin
sudo apt install gnome-software -y
sudo apt remove gnome-software-plugin-snap -y
sudo apt remove --purge snapd -y
sudo apt-mark hold snap

#Cleanup packages
sudo apt autoremove -y
